typedef struct node{
	int val;
	struct node* next;
}node;
typedef struct list{
	node *head;
}

void init(list* l);
void insert(list* l,int val);
void print(list* l);
void segregate(list* l);
void swap(int a,int b);
